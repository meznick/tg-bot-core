import functools
from argparse import ArgumentParser
from logging import getLogger, FileHandler, StreamHandler, Formatter
from os import environ

from sqlalchemy import create_engine
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import sessionmaker

from .config import LOGGER_NAME, LOG_LEVEL, LOG_FILE, PROXY_HOST, PROXY_PORT, PROXY_AUTH
from .core.bot import TelegramBot
from .core.menu import MainMenu
from .core.user import User, UserSession

DB_URI = '{db_conn_type}://{database_auth}@{database_host}/{database_name}'


parser = ArgumentParser('TELEGRAM')
parser.add_argument('-T', '--telegram', help='Telegram Bot API key.', required=False)
parser.add_argument('-H', '--host', dest='proxy_host',
                    help='Address of proxy host (optional).', required=False)
parser.add_argument('-P', '--port',  dest='proxy_port',
                    help='Port of proxy host (optional).', required=False)
parser.add_argument('-C', dest='proxy_auth',
                    help='Proxy auth credentials formatted as usr:pwd.')
parser.add_argument('--db_conn_type', dest='db_conn_type', help='Database connection type.')
parser.add_argument('-D', '--db_host', dest='database_host', help='Database server address.')
parser.add_argument('-N', '--db_name', dest='database_name', help='Database name.')
parser.add_argument('--database_auth', dest='database_auth',
                    help='Database auth credentials formatted as usr:pwd:dbname.')
parser.add_argument('--debug', dest='debug')
args = parser.parse_args()


def collect_environment(required: list, optional: list) -> dict:
    """
    Collects environment and arguments required for module to work.
    If some of variables are unavailable raises exception.
    """
    environment = dict()
    for variable in required:
        try:
            environment.update({variable: environ[variable.upper()]})
        except KeyError:
            try:
                environment.update({variable: getattr(args, variable)})
            except AttributeError:
                raise Exception(f'No variable {variable} provided')
    for variable in optional:
        try:
            environment.update({variable: environ[variable.upper()]})
        except KeyError:
            try:
                environment.update({variable: getattr(args, variable)})
            except AttributeError:
                environment.update({variable: None})

    if environment['proxy_host'] is None and PROXY_HOST != '':
        environment['proxy_host'] = PROXY_HOST
    if environment['proxy_port'] is None and PROXY_PORT != '':
        environment['proxy_port'] = PROXY_PORT
    if environment['proxy_auth'] is None and PROXY_AUTH != '':
        environment['proxy_auth'] = PROXY_AUTH

    if environment['logger_name'] is None and LOGGER_NAME != '':
        environment['logger_name'] = LOGGER_NAME
    if environment['log_level'] is None:
        environment['log_level'] = LOG_LEVEL
    if environment['log_file'] is None and LOG_FILE != '':
        environment['log_level'] = LOG_FILE

    if environment['debug'] in ('true', 'True'):
        environment['debug'] = True
    else:
        environment['debug'] = False
    return environment


class AppTemplate:
    def __init__(self, config):
        """
        App should init bot and additional modules (optional).
        """
        self.logger = self.prepare_logger(config)
        self.bot = TelegramBot(config)
        self.db_engine = create_engine(DB_URI.format(
            db_conn_type=config['db_conn_type'],
            database_auth=config['database_auth'],
            database_host=config['database_host'],
            database_name=config['database_name']
            ),
            echo=config['debug'])
        self.logger.info(f'Connected to DB!')
        self.sessions = []
        # in development -- menu module
        self.menu = MainMenu()
        # in development -- inline module
        self.inline = None

    @staticmethod
    def prepare_logger(config):
        log = getLogger(config['logger_name'])
        log.setLevel(int(config['log_level']))
        fm = Formatter(u'%(asctime)s - %(levelname)s - %(message)s')
        if config['log_file'] is not None:
            fh = FileHandler(config['log_file'], mode='w+', encoding='utf-8')
            fh.setLevel(int(config['log_level']))
            fh.setFormatter(fm)
            log.addHandler(fh)
        sh = StreamHandler()
        sh.setFormatter(fm)
        log.addHandler(sh)
        log.info(f'Logger initiated with level {config["log_level"]}!')
        return log

    def logging_decorator(self):
        def decorator_outer(func):
            @functools.wraps(func)
            def decorator(*a, **kw):
                message = a[0]
                try:
                    # if user sent message as text
                    user_sent = f'message: {message.text}'
                except AttributeError:
                    # if user sent data by pressing buttons
                    user_sent = f'data: {message.data}'
                self.logger.debug(f'User {message.from_user.id} sent {user_sent}.')
                task = func(*a, **kw)
                if task is not None:
                    while not task.done:
                        task.wait()
                    self.logger.debug(f'Bot responded: {task.result.json}.')
                else:
                    self.logger.debug('No task to process!')
                return task
            return decorator
        return decorator_outer

    def uses_database_session(self):
        def decorator_outer(func):
            @functools.wraps(func)
            def decorator(*a, **kw):
                session = sessionmaker(bind=self.db_engine)()
                result = func(*a, **kw, db_session=session)
                session.commit()
                session.close()
                return result
            return decorator
        return decorator_outer

    def start(self):
        """
        Contains user action handlers.
        """
        @self.bot.bot.message_handler(content_types=['text'])
        @self.logging_decorator()
        @self.uses_database_session()
        def process_messages(msg, db_session):
            try:
                user = db_session.query(User).filter(User.id == msg.from_user.id).one()
            except NoResultFound:
                # user can be missing in database (new user/bug)
                user = User(
                    id=msg.from_user.id,
                    lang='EN'
                    # todo: add more fields
                )
                db_session.add(user)
            session = UserSession(user)
            # todo: add user session cache
            return self.bot.process_messages(msg, session, self.menu)
        self.bot.bot.polling()

    def stop(self):
        """
        Dump all data for next run:
        - user sessions
        - bot state
        """
        pass


if __name__ == '__main__':
    env = collect_environment(required=[
        'telegram',
        'db_conn_type', 'database_host', 'database_name', 'database_auth'
    ], optional=[
        'proxy_port', 'proxy_host', 'proxy_auth', 'database',
        'logger_name', 'log_level', 'log_file', 'debug'
    ])
    print(env)
    app = AppTemplate(env)
    app.start()
    app.stop()
