# -*- coding: utf-8 -*-
from logging import getLogger
from traceback import print_tb
from typing import Optional

import telebot
from telebot import apihelper

from ..config import LOGGER_NAME
from ..core.menu import MainMenu
from ..core.user import UserSession


class TelegramBot:
    bot = None

    def __init__(self, config: dict):
        self.log = getLogger(LOGGER_NAME)
        proxy = {'host': config['proxy_host'],
                 'port': config['proxy_port']}
        try:
            proxy.update({'auth': config['proxy_auth'] + '@'})
            apihelper.proxy = {
                'https': f"socks5h://{proxy['auth']}{proxy['host']}:{proxy['port']}"
            }
            self.log.info(f"Using proxy: {apihelper.proxy}")
        except TypeError:
            self.log.info(f"Not using proxy")
        self.bot = telebot.AsyncTeleBot(token=config['telegram'])
        me = self.bot.get_me().wait()
        if not issubclass(type(me), telebot.types.User):
            self.log.error('Telegram API was not initiated!')
            raise me[0](print_tb(me[2]))
        self.log.info('Telegram API initiated!')

    def process_messages(self, msg: telebot.types.Message,
                         session: UserSession, menu: Optional[MainMenu] = None):
        if msg.text[0] == '/':
            return self._process_commands(msg, session, menu)

    def _process_commands(self, msg, session, menu):
        if msg.text == '/start':
            # return self.bot.send_message(
            #     chat_id=msg.chat.id,
            #     text=menu.menus['start_screen'].title.format(user=session),
            #     reply_markup=menu.menus['start_screen'].keyboard
            # )
            return self.bot.send_message(
                chat_id=msg.chat.id,
                text='HELLO WORLD!'
            )
