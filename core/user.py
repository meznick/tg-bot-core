# -*- coding: utf-8 -*-
from logging import getLogger

from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('postgresql+psycopg2://tg_bot:vuv1AtOKdmQ@172.18.33.204/telegram',
                       echo=True)
Base = declarative_base()
log = getLogger('ludomanych')


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String)
    first_name = Column(String)
    last_name = Column(String)
    lang = Column(String(2))
    state = Column(String)


class UserSession:
    def __init__(self, user: User):
        self.user = user
        self.status = ''


if __name__ == '__main__':
    Base.metadata.create_all(engine)
